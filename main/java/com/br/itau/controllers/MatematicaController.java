package com.br.itau.controllers;

import com.br.itau.models.Matematica;
import com.br.itau.services.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PostMapping("soma")
    public Integer soma(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if(numeros.isEmpty() || numeros.size() < 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Para somar, é necessário informar dois números pelo menos");
        }
        return matematicaService.soma(numeros);
    }

    @PostMapping("subtrai")
    public Integer subtrai(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if(numeros.isEmpty() || numeros.size() < 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Para subtrair, é necessário informar dois números pelo menos");
        }
        return matematicaService.subtrai(numeros);
    }

    @PostMapping("multiplica")
    public Integer multiplica(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if(numeros.isEmpty() || numeros.size() < 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Para multiplicar, é necessário informar dois números pelo menos");
        }
        return matematicaService.multiplica(numeros);
    }

    @PostMapping("divide")
    public Integer divide(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();

        if(numeros.isEmpty() || numeros.size() != 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Para dividir, é necessário apenas dois números");
        }

        return matematicaService.divide(numeros);
    }

}
