package com.br.itau.services;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MatematicaService {
    public int soma(List<Integer> numeros){
        int resultado = 0;
        for (int numero:numeros){
            resultado += numero;
        }
        return resultado;
    }
    public int subtrai(List<Integer> numeros){
        int resultado = 0;
        for (int numero:numeros){
            if (numeros.indexOf(numero) == 0){
                resultado = numero;
            }
            else{
                resultado -= numero;}
        }
        return resultado;
    }
    public int multiplica(List<Integer> numeros){
        int resultado = 1;
        for (int numero:numeros){
            resultado *= numero;
        }
        return resultado;
    }
    public int divide(List<Integer> numeros){
        int resultado = 0;
        int divisor = 0;
        int dividendo = 0;

        for (int numero:numeros){
            if (numero > dividendo){
                divisor = dividendo;
                dividendo = numero;
            }
            else{
                divisor = numero;
            }
        }
        resultado = dividendo / divisor;
        return resultado;
    }
}
